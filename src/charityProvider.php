<?php

namespace RaceNation\Fundraising;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class CharityProvider
{

    protected $appId;

    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    public function searchCharities($q)
    {
        $response =  $this->vendorSearchCharities($q);
        if ($response['status_code'] == 200) {
            $charities = $this->extractCharities($response['body']);
        } else {
            $charities = [];
        }
        return $charities;
    }

    public function getCharity($charityId)
    {
        $response = $this->vendorGetCharity($charityId);
        if ($response['status_code'] == 200) {
            $charity = $this->parseCharity($response['body']);
        } else {
            $charity = null;
        }
        return $charity;
    }

    protected function callApi($uri)
    {
        $client = new Client();

        try {
            $res = $client->request('GET', $this->buildUri($uri), [
                'headers' => [
                    'Accept'     => 'application/json',
                ]
            ]);

            
        } catch (ClientException $e) {
            
            $res = $e->getResponse();
        }

        $response = [
            'status_code' =>  $res->getStatusCode(),
            'body' => (string) $res->getBody(),
        ];
       
        return $response;
    }
}
