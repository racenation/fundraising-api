<?php

namespace RaceNation\Fundraising;

class VirginMoneyGivingUserProvider extends UserProvider
{
    protected $baseUri = 'https://sandbox.api.virginmoneygiving.com';

    protected function vendorSearchUsers($email, $name, $dob)
    {
        $uri = '/fundraisers/v1/validateaccount.json?emailAddress=' . $email . '&dateOfBirth=' . $dob;
        return $this->callApi($uri);
    }

    protected function extractUsers($json)
    {

        $users = [];
        $results = json_decode($json, true);
        if ($results['accountExistsIndicator']) {
            die('Implement User Choice Here');
        } else {
            $users = false;
        }
        return $users;
    }

    protected function buildUri($uri)
    {
        return $this->baseUri . $uri . "&api_key=" . $this->appId;
    }

}