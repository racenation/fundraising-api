<?php

namespace RaceNation\Fundraising;

class JustGivingFundraisingPageProvider extends FundraisingPageProvider
{
    protected $baseUri = 'https://api.justgiving.com';

    protected function vendorCreatePage($body)
    {
        $uri = '/v1/fundraising/pages';
        $method = 'PUT';
        $body = json_encode($body);
        $response = $this->callApi($uri, $body, $method);
        return $response;
    }

    protected function buildUri($uri)
    {
        return $this->baseUri . '/' . $this->appId . $uri;
    }

}