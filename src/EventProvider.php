<?php

namespace RaceNation\Fundraising;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class EventProvider
{

    protected $appId;

    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    public function searchEvents($q)
    {
        $response = $this->vendorSearchEvents($q);
        if ($response['status_code'] == 200) {
            $events = $this->extractEvents($response['body']);
        } else {
            $events = [];
        }
        return $events;
    }

    public function getEvent($eventId)
    {
        $response = $this->vendorGetEvent($eventId);
        if ($response['status_code'] == 200) {
            $event = $this->parseEvent($response['body']);
        } else {
            $event = null;
        }
        return $event;
    }

    protected function callApi($uri)
    {
        $client = new Client();
        try {
            $res = $client->request('GET', $this->buildUri($uri), [
                'headers' => [
                    'Accept'     => 'application/json',
                ]
            ]);

            
        } catch (ClientException $e) {
            
            $res = $e->getResponse();
        }

        $response = [
            'status_code' =>  $res->getStatusCode(),
            'body' => (string) $res->getBody(),
        ];

        return $response;
    }
}
