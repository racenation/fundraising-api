<?php

namespace RaceNation\Fundraising;

use Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

class FundraisingPageProvider
{
    protected $appId;
    protected $username;
    protected $password;

    public function __construct($username, $password, $appId)
    {
        $this->username = $username;
        $this->password = $password;
        $this->appId = $appId;
    }

    public function createPage($attr)
    {
        $response =  $this->vendorCreatePage($attr);

        return $response;

    }

    protected function callApi($uri, $body = [], $method = 'GET')
    {
        $client = new Client();
        Log::debug('Calling ' . $this->buildUri($uri));
        try {
            switch ($method) {
                case 'GET':
                    $res = $client->request('GET', $this->buildUri($uri), [
                        'headers' => [
                            'Accept' => 'application/json',
                        ]
                    ]);
                    break;
                case 'POST':
                    $res = $client->request('POST', $this->buildUri($uri), [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'auth' => [
                            $this->username, 
                            $this->password, 
                        ],
                        'body' => $body,
                    ]);
                    break;

                case 'PUT':
                    $res = $client->request('PUT', $this->buildUri($uri), [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'auth' => [
                            $this->username, 
                            $this->password, 
                        ],
                        'body' => $body,
                    ]);
                    break;
                
                default:
                    throw new Exception("Unknown HTTP method " . $method, 1);
                    break;
            }
                    
        } catch (ClientException $e) {
            
            $res = $e->getResponse();
        } catch (ServerException $e) {
            var_dump($e);
            die;
        }

        $response = [
            'status_code' =>  $res->getStatusCode(),
            'body' => (string) $res->getBody(),
        ];
       
        return $response;
    }
}