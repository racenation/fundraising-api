<?php

namespace RaceNation\Fundraising;

class JustGivingUserProvider extends UserProvider
{
    protected $baseUri = 'https://api.justgiving.com';

    protected function vendorSearchUsers($email, $name, $dob)
    {
        $uri = '/v1/fundraising/search?q=' . $name;
        return $this->callApi($uri);
    }

    protected function extractUsers($json)
    {

        $users = [];
        $results = json_decode($json, true);
        dd($results);
        if (count($results['events']) > 0) {
            foreach ($results['events'] as $event) {
                $tmpUser = new \stdClass;
                $tmpUser->name = ucfirst($event['name']);
                $tmpUser->date = $this->parseDate($event['startDate']);
                $tmpUser->friendly_date = $this->parseDate($event['startDate'])->format('dS M Y');
                $tmpUser->id = $event['id'];
                $users[] = $tmpUser;
            }
        }
        return $users;
    }

    protected function vendorCreateUser($username, $password, $entrant, $address)
    {

        $uri = '/v1/account';
        $method = 'PUT';
        $reference = 'rn-' . $entrant->id . '-jg';
        $body = [
            'acceptTermsAndConditions' => true,
            'address' => [
                "country" => $address->country->name,
                "countyOrState" => $address->line_3_locality,
                "line1" => $address->line_1_number_building,
                "line2" => $address->line_2_street,
                "postcodeOrZipcode" => $address->postcode,
                "townOrCity" => $address->city,
            ],
            "email" => $username,
            "title" =>$entrant->title,
            "firstName" => $entrant->first_name,
            "lastName" => $entrant->last_name,
            "password" => $password,
            "reference" => $reference,
        ];

        $body = json_encode($body);
        $response = $this->callApi($uri, $body, $method);
        if (isset($response['status_code']) && $response['status_code'] == 200) {
            return true;
        } else {
            return false;
        }
    }

    protected function vendorAuthUser($username, $password)
    {
        $uri = '/v1/account/validate';
        $method = 'POST';
        $body = [
            'email' => $username,
            'password' => $password,
        ];

        $body = json_encode($body);
        $response = $this->callApi($uri, $body, $method);
        if (isset($response['status_code']) && $response['status_code'] == 200) {
            $body = json_decode($response['body'], true);
            if (isset($body['isValid'])) {
                return $body['isValid'];
            } else {
                return false;
            }
        } else {
            return false;
        }
      
        
        return false;

    }

    protected function buildUri($uri)
    {
        return $this->baseUri . '/' . $this->appId . $uri;
    }

}