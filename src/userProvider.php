<?php

namespace RaceNation\Fundraising;

use Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;

class UserProvider
{
    protected $appId;

    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    public function searchUsers($email, $name, $dob)
    {
        $response =  $this->vendorSearchUsers($email, $name, $dob);
        if ($response['status_code'] == 200) {
            $users = $this->extractUsers($response['body']);
        } else {
            $users = [];
        }
        return $users;
    }

    public function createUser($username, $password, $entrant, $address) 
    {
        $response = $this->vendorCreateUser($username, $password, $entrant, $address);
        return $response;
    }

    public function authUser($username, $password) 
    {
        $response = $this->vendorAuthUser($username, $password);
        return $response;
    }

    protected function callApi($uri, $body = [], $method = 'GET')
    {
        $client = new Client();
        Log::debug('Calling ' . $this->buildUri($uri));
        try {
            switch ($method) {
                case 'GET':
                    $res = $client->request('GET', $this->buildUri($uri), [
                        'headers' => [
                            'Accept' => 'application/json',
                        ]
                    ]);
                    break;
                case 'POST':
                    $res = $client->request('POST', $this->buildUri($uri), [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'body' => $body,
                    ]);
                    break;

                case 'PUT':
                    $res = $client->request('PUT', $this->buildUri($uri), [
                        'headers' => [
                            'Content-Type' => 'application/json',
                            'Accept' => 'application/json',
                        ],
                        'body' => $body,
                    ]);
                    break;
                
                default:
                    throw new Exception("Unknown HTTP method " . $method, 1);
                    break;
            }
                    
        } catch (ClientException $e) {
            
            $res = $e->getResponse();
        }

        $response = [
            'status_code' =>  $res->getStatusCode(),
            'body' => (string) $res->getBody(),
        ];
       
        return $response;
    }

}