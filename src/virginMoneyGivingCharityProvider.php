<?php

namespace RaceNation\Fundraising;

use \DateTime;

class VirginMoneyGivingCharityProvider extends CharityProvider
{

    protected $baseUri = 'https://sandbox.api.virginmoneygiving.com';

    protected function vendorSearchCharities($q)
    {
        $uri = '/charities/v1/search?name=' . $q;
        return $this->callApi($uri);
    }

    protected function extractCharities($json)
    {
        $charities = [];
        $results = json_decode($json, true);
        if (count($results['charities']) > 0) {
            foreach ($results['charities'] as $charity) {
                $tmpCharity = new \stdClass;
                $tmpCharity->name = ucfirst($charity['name']);
                $tmpCharity->id = $charity['resourceId'];
                $charities[] = $tmpCharity;
            }
        }
        return $charities;
    }

    protected function vendorGetCharity($charityId)
    {
        $uri = '/charities/v1/account/' . $charityId . '.json?test=false';
        return $this->callApi($uri);
    }

    protected function parseCharity($json)
    {

        $charity = [];

        $result = json_decode($json, true);
   
        $charity['name'] = $result['name'];
        $charity['description'] = $result['description'];
        $charity['id'] = $result['resourceId'];
        $charity['completionDate'] = null;
        $charity['expiryDate'] = null;
        $charity['startDate'] = null;
        $charity['eventType'] = null;

        return $charity;
    }


    protected function vendorSearchEvents($q)
    {
        $uri = '/events/v1/search.json?name=' . $q;
        return $this->callApi($uri);
    }

    protected function extractEvents($json)
    {
        $events = [];
        $results = json_decode($json, true);
        if (count($results['events']) > 0) {
            foreach ($results['events'] as $event) {
                $tmpEvent = new \stdClass;
                $tmpEvent->name = ucfirst($event['name']);
                $tmpEvent->date = $this->parseDate($event['eventDate']);
                $tmpEvent->friendly_date = $this->parseDate($event['eventDate'])->format('dS M Y');
                $tmpEvent->id = $event['eventResourceId'];
                $events[] = $tmpEvent;
            }
        }
        return $events;
    }

    protected function vendorGetEvent($eventId)
    {
        $uri = '/events/v1/account/' . $eventId . '/summary.json';
        return $this->callApi($uri);
    }

    protected function parseEvent($json)
    {

        $charity = [];

        $result = json_decode($json, true);
        $charity['name'] = $result['name'];
        $charity['description'] = $result['description'];
        $charity['id'] = $result['resourceId'];
        $charity['registeredCharityNumber'] = $result['registeredCharityNumber'];
        $charity['logoURL'] = $result['logoURL'];
        $charity['websiteURL'] = $result['websiteURL'];

        return $charity;
    }

    protected function parseDate($date)
    {
        $date = DateTime::createFromFormat('Ymd', $date);
        return $date;
    }

    protected function buildUri($uri)
    {
        return $this->baseUri . $uri . "&api_key=" . $this->appId;
    }
}
