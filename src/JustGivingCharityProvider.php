<?php

namespace RaceNation\Fundraising;

use \DateTime;

class JustGivingCharityProvider extends CharityProvider
{

    protected $baseUri = 'https://api.justgiving.com';

    protected function vendorSearchCharities($q)
    {
        $uri = '/v1/charity/search?q=' . $q;
        return $this->callApi($uri);
    }

    protected function extractCharities($json)
    {
        $charities = [];
        $results = json_decode($json, true);
        if (count($results['charitySearchResults']) > 0) {
            foreach ($results['charitySearchResults'] as $charity) {
                $tmpCharity = new \stdClass;
                $tmpCharity->name = ucfirst($charity['name']);
                $tmpCharity->id = $charity['charityId'];
                $charities[] = $tmpCharity;
            }
        }
        return $charities;
    }

    protected function vendorGetCharity($eventId)
    {
        $uri = '/v1/charity/' . $eventId;
        return $this->callApi($uri);
    }

    protected function parseCharity($json)
    {
        $charity = [];

        $result = json_decode($json, true);
        $charity['name'] = $result['name'];
        $charity['description'] = $result['description'];
        $charity['id'] = $result['id'];

        return $charity;
    }

    protected function vendorSearchEvents($q)
    {
        $uri = '/v1/event/search?q=' . $q;
        return $this->callApi($uri);
    }

    protected function extractEvents($json)
    {
        $events = [];
        $results = json_decode($json, true);
        if (count($results['events']) > 0) {
            foreach ($results['events'] as $event) {
                $tmpEvent = new \stdClass;
                $tmpEvent->name = ucfirst($event['name']);
                $tmpEvent->date = $this->parseDate($event['startDate']);
                $tmpEvent->friendly_date = $this->parseDate($event['startDate'])->format('dS M Y');
                $tmpEvent->id = $event['id'];
                $events[] = $tmpEvent;
            }
        }
        return $events;
    }

    protected function vendorGetEvent($eventId)
    {
        $uri = '/v1/event/' . $eventId;
        return $this->callApi($uri);
    }

    protected function parseEvent($json)
    {
        $event = [];

        $result = json_decode($json, true);
        
        $event['name'] = $result['name'];
        $event['description'] = $result['description'];
        $event['id'] = $result['id'];
        $event['completionDate'] = $this->parseDate($result['completionDate']);
        $event['expiryDate'] = $this->parseDate($result['expiryDate']);
        $event['startDate'] = $this->parseDate($result['startDate']);
        $event['eventType'] = $result['eventType'];

        return $event;
    }

    protected function parseDate($date)
    {
        $dateParts = explode('+', $date);
        $timestamp = preg_replace('/\D/', '', $dateParts[0]);
        $date = DateTime::createFromFormat('U', $timestamp / 1000);
        return $date;
    }

    protected function buildUri($uri)
    {
        return $this->baseUri . '/' . $this->appId . $uri;
    }
}
