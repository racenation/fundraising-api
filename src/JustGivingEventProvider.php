<?php

namespace RaceNation\Fundraising;

use \DateTime;

class JustGivingEventProvider extends EventProvider
{

    protected $baseUri = 'https://api.justgiving.com';

    protected function vendorSearchEvents($q)
    {
      //changed uri to use the new JG one search method.
      $uri = '/v1/onesearch?q=' . $q . '&i=Event';
        //$uri = '/v1/event/search?q=' . $q;
        return $this->callApi($uri);
    }

    protected function extractEvents($json)
    {
        // coded changes required to adapt to one search method
        $events = [];
        $results = json_decode($json, true);

          if (count($results['GroupedResults'][0]['Results']) > 0) {

            foreach ($results['GroupedResults'][0]['Results'] as $event) {
                $tmpEvent = new \stdClass;
                $tmpEvent->name = ucfirst($event['Name']);
                $tmpEvent->date = $this->parseDate($event['CreatedDate']);
                $tmpEvent->friendly_date = $this->parseDate($event['CreatedDate'])->format('dS M Y');
                $tmpEvent->id = $event['Id'];
                $events[] = $tmpEvent;
            }
        }
        return $events;
    }

    protected function vendorGetEvent($eventId)
    {
        $uri = '/v1/event/' . $eventId;
        return $this->callApi($uri);
    }

    protected function parseEvent($json)
    {
        $event = [];

        $result = json_decode($json, true);

        $event['name'] = $result['name'];
        $event['description'] = $result['description'];
        $event['id'] = $result['id'];
        $event['completionDate'] = $this->parseDate($result['completionDate']);
        $event['expiryDate'] = $this->parseDate($result['expiryDate']);
        $event['startDate'] = $this->parseDate($result['startDate']);
        $event['eventType'] = $result['eventType'];

        return $event;
    }

    protected function parseDate($date)
    {
        $dateParts = explode('+', $date);
        $timestamp = preg_replace('/\D/', '', $dateParts[0]);
        $date = DateTime::createFromFormat('U', $timestamp / 1000);
        return $date;
    }

    protected function buildUri($uri)
    {
        return $this->baseUri . '/' . $this->appId . $uri;
    }
}
